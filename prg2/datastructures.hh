// Datastructures.hh

#ifndef DATASTRUCTURES_HH
#define DATASTRUCTURES_HH

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <limits>

// Type for beacon IDs
using BeaconID = std::string;

// Return value for cases where required beacon was not found
BeaconID const NO_ID = "----------";

// Return value for cases where integer values were not found
int const NO_VALUE = std::numeric_limits<int>::min();

// Return value for cases where name values were not found
std::string const NO_NAME = "-- unknown --";

// Type for a coordinate (x, y)
struct Coord
{
    int x = NO_VALUE;
    int y = NO_VALUE;
};

// Example: Defining == and hash function for Coord so that it can be used
// as key for std::unordered_map/set, if needed
inline bool operator==(Coord c1, Coord c2) { return c1.x == c2.x && c1.y == c2.y; }
inline bool operator!=(Coord c1, Coord c2) { return !(c1==c2); } // Not strictly necessary

struct CoordHash
{
    std::size_t operator()(Coord xy) const
    {
        auto hasher = std::hash<int>();
        auto xhash = hasher(xy.x);
        auto yhash = hasher(xy.y);
        // Combine hash values (magic!)
        return xhash ^ (yhash + 0x9e3779b9 + (xhash << 6) + (xhash >> 2));
    }
};

// Example: Defining < for Coord so that it can be used
// as key for std::map/set
inline bool operator<(Coord c1, Coord c2)
{
    if (c1.y < c2.y) { return true; }
    else if (c2.y < c1.y) { return false; }
    else { return c1.x < c2.x; }
}

// Return value for cases where coordinates were not found
Coord const NO_COORD = {NO_VALUE, NO_VALUE};

// Type for color (RGB)
struct Color
{
    int r = NO_VALUE;
    int g = NO_VALUE;
    int b = NO_VALUE;
};

// Equality and non-equality comparisons for Colors
inline bool operator==(Color c1, Color c2) { return c1.r == c2.r && c1.g == c2.g && c1.b == c2.b; }
inline bool operator!=(Color c1, Color c2) { return !(c1==c2); }

// Return value for cases where color was not found
Color const NO_COLOR = {NO_VALUE, NO_VALUE, NO_VALUE};

// Type for light transmission cost (used only in the second assignment)
using Cost = int;

// Return value for cases where cost is unknown
Cost const NO_COST = NO_VALUE;


// This is the class you are supposed to implement

class Datastructures
{
public:
    Datastructures();
    ~Datastructures();

    // Estimate of performance: O(1)
    // Short rationale for estimate: unordered_map.size() is constant complexity
    int beacon_count();

    // Estimate of performance: O(n)
    // Short rationale for estimate:
    // - unordered_map.clear() is linear complexity
    // - make_pair is constant complexity
    void clear_beacons();

    // Estimate of performance: O(n)
    // Short rationale for estimate:
    // - going through all elements in unordered_map in a for loop is linear complexity
    // - vector.push_back() is constant complexity in amortized time
    std::vector<BeaconID> all_beacons();

    // Estimate of performance: O(1)
    // Short rationale for estimate:
    // - all operations are done only once
    // - get_brightness() is constant complexity
    bool add_beacon(BeaconID id, std::string const& name, Coord xy, Color color);

    // Estimate of performance: O(1) (on average)
    // Short rationale for estimate:
    // - finding a certain value (which count does) from unordered_map is constant complexity on average
    std::string get_name(BeaconID id);

    // Estimate of performance: O(1) (on average)
    // Short rationale for estimate: same as get_name
    Coord get_coordinates(BeaconID id);

    // Estimate of performance: O(1) (on average)
    // Short rationale for estimate: same as get_name
    Color get_color(BeaconID id);

    // Estimate of performance: O(n*log(n))
    // Short rationale for estimate:
    // - all_beacons() is linear complexity as previoisly stated
    // - std::sort() is O(n*log(n)) complexity
    // - the lambda function used in the sort is constant complexity since
    // - vector.at() is constant complexity
    std::vector<BeaconID> beacons_alphabetically();

    // Estimate of performance: O(n*log(n))
    // Short rationale for estimate:
    // - all_beacons() linear complexity
    // - std::sort() O(n*log(n)) complexity
    // - lambda function used constant complexity
    // - vector.front() and .back() are constant complexity, so the remaining operations are also constant complexity
    std::vector<BeaconID> beacons_brightness_increasing();

    // Estimate of performance: O(n*log(n)) if min_b is invalidated often, otherwise O(1) (on average)
    // Short rationale for estimate:
    // - if minimum brightness is invalidated, the function calls beacons_brightness_increasing to update it (resulting in O(n*log(n)) complexity)
    // - otherwise the minimum brightness is already known and it is returned (constant complexity)
    BeaconID min_brightness();

    // Estimate of performance: O(n*log(n)) if min_b is invalidated often, otherwise O(1) (on average)
    // Short rationale for estimate: same as min_brightness()
    BeaconID max_brightness();

    // Estimate of performance: O(n*log(n)); normally O(n)
    // Short rationale for estimate:
    // - going through all elements in unordered_map in a for loop is linear complexity
    // - std::sort is O(n*log(n)) but since most of the time beacons with the same names are very rare results in almost constant complexity sorting time
    std::vector<BeaconID> find_beacons(std::string const& name);

    // Estimate of performance: O(1) (on average)
    // Short rationale for estimate:
    // - unordered_map.count() and .at() are constant complexity on average
    bool change_beacon_name(BeaconID id, std::string const& newname);

    // Estimate of performance: O(1) (on average)
    // Short rationale for estimate:
    // - unordered_map.count() and .at() are constant complexity on average
    // - get_brightness() is constant complexity
    bool change_beacon_color(BeaconID id, Color newcolor);

    // Estimate of performance: O(1) (on average)
    // Short rationale for estimate:
    // - unordered_map.count() and .at() are constant complexity on average
    // - vector.push_back() is constant complexity in amortized time
    bool add_lightbeam(BeaconID sourceid, BeaconID targetid);

    // Estimate of performance: O(n*log(n)); normally O(1) (on average)
    // Short rationale for estimate:
    // - unordered_map.count() and .at() are constant complexity on average
    // - std::sort is O(n*log(n)) but since only a very small proportion of other beacons light the given beacon usually results in almost constant complexity sorting time
    std::vector<BeaconID> get_lightsources(BeaconID id);

    // Estimate of performance: O(n)
    // Short rationale for estimate:
    // - unordered_map.count() and .at() are constant complexity on average
    // - vector.push_back is constant complexity in amortized time
    // - the while-loop is linear complexity
    std::vector<BeaconID> path_outbeam(BeaconID id);

    // Non-compulsory operations

    // Estimate of performance: O(n)
    // Short rationale for estimate:
    // - std::remove and vector.erase() are linear complexity
    // - for loop is linear complexity
    bool remove_beacon(BeaconID id);

    // Estimate of performance:
    // Short rationale for estimate:
    std::vector<BeaconID> path_inbeam_longest(BeaconID id);

    // Estimate of performance:
    // Short rationale for estimate:
    Color total_color(BeaconID id);

    // Phase 2 operations

    // Estimate of performance: O(n*log(n))
    // Short rationale for estimate: std::sort is O(n*log(n))
    std::vector<Coord> all_xpoints();

    // Estimate of performance: O(n)
    // Short rationale for estimate:
    // - std::hash is assumed O(1)
    // - std::unordered_map .find() is O(1) on average
    // - std::find is O(n)
    // - std::vector .push_back() is O(n) if reallocation happens and O(1) if not. Reallocation is rare, thus O(1)
    // - std::unordered_map .insert() is O(1) on average
    // NOTE: std::find for the query vectors drop the performance to O(n), otherwise it would be O(1)
    bool add_fibre(Coord xpoint1, Coord xpoint2, Cost cost);

    // Estimate of performance: O(n*log(n))
    // Short rationale for estimate:
    // - std::hash is assumed O(1)
    // - iteration through the fibres of xpoint is O(n)
    // - std::make_pair is O(1)
    // - std::vector .push_back() is O(n) if reallocation happens and O(1) if not. Reallocation is rare, thus O(1)
    // - std::sort is O(n*log(n))
    std::vector<std::pair<Coord, Cost>> get_fibres_from(Coord xpoint);

    // Estimate of performance: O(n*log(n))
    // Short rationale for estimate: std::sort is n*log(n)
    std::vector<std::pair<Coord, Coord>> all_fibres();

    // Estimate of performance: O(n)
    // Short rationale for estimate:
    // - std::hash is assumed O(1)
    // - std::make_pair is O(1)
    // - std::unordered_map .find() is O(1) on average
    // - std::unordered_map .erase() is O(1) on average
    // - std::remove is O(n)
    // - std::vector .erase() is O(n)
    // NOTE: handling of the query vectors drop the performance to O(n), otherwise it would be O(1)
    bool remove_fibre(Coord xpoint1, Coord xpoint2);

    // Estimate of performance: O(n)
    // Short rationale for estimate:
    // - std::unordered_map .clear() is O(n)
    // - std::vector .clear() is O(n)
    void clear_fibres();

    // Estimate of performance: O(n^2)
    // Short rationale for estimate:
    // - std::make_pair is O(1)
    // - std::hash is assumed O(1)
    // - complexity of while loop is O(d); here d is constant so O(1)
    //   - for loop for each route is O(n)
    //     - std::vector .back() is O(1)
    //     - std::pair .first and .second are O(1)
    //     - iteration through the fibres of xpoint is O(n)
    //       - iterator .second is O(1)
    //       - std::unordered_map .find() is O(1) on average
    //       - std::unordered_map .insert() is O(1) on average
    //       - std::vector .push_back() is O(n) if reallocation happens and O(1) if not. Reallocation is rare, thus O(1)
    // Content of the while loop amounts to O(n^2)
    std::vector<std::pair<Coord, Cost>> route_any(Coord fromxpoint, Coord toxpoint);

    // Non-compulsory operations

    // Estimate of performance: O(n^2)
    // Short rationale for estimate: Exactly the same as route_any
    std::vector<std::pair<Coord, Cost>> route_least_xpoints(Coord fromxpoint, Coord toxpoint);

    // Estimate of performance: O(n^3)
    // Short rationale for estimate:
    // - std::make_pair is O(1)
    // - complexity of while loop is O(d); here d is constant so O(1)
    //   - for loop for each route is O(n)
    //     - std::vector .back() is O(1)
    //     - std::pair .first and .second are O(1)
    //     - std::hash is assumed O(1)
    //     - iteration through the fibres of xpoint is O(n)
    //       - iterator .second is O(1)
    //       - std::find_if is O(n)
    //         - the lambda function inside the find_if statement is O(1)
    //       - std::vector .push_back() is O(n) if reallocation happens and O(1) if not. Reallocation is rare, thus O(1)
    // Content of the while loop amounts to O(n^3)
    // - the latter 2 for-loops  are both O(n)
    std::vector<std::pair<Coord, Cost>> route_fastest(Coord fromxpoint, Coord toxpoint);

    // Estimate of performance: O(n^2)
    // Short rationale for estimate:
    // - complexity of while loop is O(d); here d is constant so O(1)
    //   - for loop for each route is O(n)
    //     - std::vector .back() is O(1)
    //     - std::hash is assumed O(1)
    //     - iteration through the fibres of xpoint is O(n)
    //       - iterator .second is O(1)
    //       - std::unordered_map .find() is O(1) on average
    //       - std::vector .push_back() is O(n) if reallocation happens and O(1) if not. Reallocation is rare, thus O(1)
    //       - std::vector .size() is O(1)
    // Content of the while loop amounts to O(n^2)
    std::vector<Coord> route_fibre_cycle(Coord startxpoint);

    // Estimate of performance:
    // Short rationale for estimate:
    Cost trim_fibre_network();

private:

    //Contains all desired information, except id, which is stored in upper
    //structure for convenience and efficiency.
    //Also contains brightness,
    //              id of other beacon (send light),
    //              vector of other beacon ids (receive light).
    struct Beacon {
        std::string name;
        Coord crds;
        Color color;

        int brightness;

        BeaconID lightbeam_send;
        std::vector<BeaconID> lightbeams_receive;
    };

    // Calculate the brightness of a color
    int get_brightness(Color color);

    //Flag for knowing if min/max brightness are invalidated. The invalidation happens when
    // - new beacon is added
    // - beacon color is changed
    // - beacon is removed
    bool minmax_b_valid;

    //Variables for min and max brightness and their IDs
    std::pair<BeaconID,int> min_b;
    std::pair<BeaconID,int> max_b;

    //The chosen data structure
    std::unordered_map<BeaconID, Beacon> beacons;



    //fibre_ contains the information of it's end xpoint and cost
    //fibre_'s start point is given as a hash of xpoint's key
    struct fibre_ {
        Coord endx;
        Cost cost;
    };

    //xpoint_ contains data of it's fibres which are unique by the hash generated from their end xpoint's coordinates
    struct xpoint_ {
        std::unordered_map<int,fibre_> fibres;
    };

    //xpoints is the primary container of all data related to the fibres
    //it contains all the xpoints by the hash of their coordinates
    std::unordered_map<int, xpoint_> xpoints;

    //v_xpoints is the vector containing xpoints for all_xpoints() query
    std::vector<Coord> v_xpoints;
    //v_fibres is the vector containing fibres for all_fibres() and get_fibres_from() queries
    std::vector<std::pair<Coord, Coord>> v_fibres;

    //the vectors are kinda redundant, but save some processing time as no transformations are needed for the mentioned queries from the primary container (xpoints)

};

#endif // DATASTRUCTURES_HH
