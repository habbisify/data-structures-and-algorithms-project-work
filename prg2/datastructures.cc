// Datastructures.cc

#include "datastructures.hh"

#include <random>
#include <algorithm>
#include <iterator>
#include <cmath>

std::minstd_rand rand_engine; // Reasonably quick pseudo-random generator

template <typename Type>
Type random_in_range(Type start, Type end)
{
    auto range = end-start;
    ++range;

    auto num = std::uniform_int_distribution<unsigned long int>(0, range-1)(rand_engine);

    return static_cast<Type>(start+num);
}

// Modify the code below to implement the functionality of the class.
// Also remove comments from the parameter names when you implement
// an operation (Commenting out parameter name prevents compiler from
// warning about unused parameters on operations you haven't yet implemented.)

Datastructures::Datastructures()
{
    //Values that are lower and higher than the possible min and max
    max_b = make_pair(NO_ID,-1);
    min_b = make_pair(NO_ID,2551);

    minmax_b_valid = false;
}

Datastructures::~Datastructures()
{
    clear_beacons();
}

int Datastructures::beacon_count()
{
    int s = beacons.size();
    if (s > 0) {
        return s;
    } else {
        return NO_VALUE;
    }
}

void Datastructures::clear_beacons()
{
    beacons.clear();

    //Need to reset the private variables
    max_b = make_pair(NO_ID,-1);
    min_b = make_pair(NO_ID,2551);

    minmax_b_valid = false;
}

std::vector<BeaconID> Datastructures::all_beacons()
{
    std::vector<BeaconID> v_beacons{};
    for ( auto it = beacons.begin(); it != beacons.end(); ++it ) {
        v_beacons.push_back(it->first); //first is the ID of the beacon
    }
    return v_beacons;
}

int Datastructures::get_brightness(Color color) {

    int brightness = 3*color.r + 6*color.g + color.b; //rough estimate on how a human sees the color brightness

    return brightness;
}

bool Datastructures::add_beacon(BeaconID id, const std::string& name, Coord xy, Color color)
{
    if (beacons.count(id) == 0) {

        Beacon beacon;
        beacon.name = name;
        beacon.crds = xy;
        beacon.color = color;

        beacon.brightness = get_brightness(color);
        minmax_b_valid = false;

        beacon.lightbeam_send = NO_ID;
        beacon.lightbeams_receive = {};

        beacons.insert(make_pair(id, beacon));
        return true;
    } else {
        return false;
    }
}

std::string Datastructures::get_name(BeaconID id)
{
    //beacons.contains(id) would be the more modern approach but isn't available until c++20
    if (beacons.count(id) == 1) {
        return beacons.at(id).name;
    } else {
        return NO_NAME;
    }
}

Coord Datastructures::get_coordinates(BeaconID id)
{
    if (beacons.count(id) == 1) {
        return beacons.at(id).crds;
    } else {
        return NO_COORD;
    }
}

Color Datastructures::get_color(BeaconID id)
{
    if (beacons.count(id) == 1) {
        return beacons.at(id).color;
    } else {
        return NO_COLOR;
    }
}

std::vector<BeaconID> Datastructures::beacons_alphabetically()
{
    std::vector<BeaconID> v_beacons = all_beacons();

    //The & in the square brackets gives the lambda function private variables of Datastructures making the comparison possible
    std::sort(v_beacons.begin(), v_beacons.end(), [&](BeaconID left, BeaconID right) -> bool
    {
        return beacons.at(left).name < beacons.at(right).name;
    });
    return v_beacons;
}

std::vector<BeaconID> Datastructures::beacons_brightness_increasing()
{
    std::vector<BeaconID> v_beacons = all_beacons();

    std::sort(v_beacons.begin(), v_beacons.end(), [&](BeaconID left, BeaconID right) -> bool
    {
        return beacons.at(left).brightness < beacons.at(right).brightness;
    });

    //Max and min brightness are gotten "free" here
    if (v_beacons.size() > 0) {
        min_b.first = v_beacons.front();
        min_b.second = beacons.at(v_beacons.front()).brightness;
        max_b.first = v_beacons.back();
        max_b.second = beacons.at(v_beacons.back()).brightness;
    }
    minmax_b_valid = true;

    return v_beacons;
}

BeaconID Datastructures::min_brightness()
{
    if (!minmax_b_valid) {
        beacons_brightness_increasing(); //updates the min_b variable
    }
    return min_b.first;
}

BeaconID Datastructures::max_brightness()
{
    if (!minmax_b_valid) {
        beacons_brightness_increasing(); //updates the max_b variable
    }
    return max_b.first;
}

std::vector<BeaconID> Datastructures::find_beacons(std::string const& name)
{
    std::vector<BeaconID> v_beacons{};
    for ( auto it = beacons.begin(); it != beacons.end(); ++it ) {
        if (it->second.name == name) {
            v_beacons.push_back(it->first);
        }
    }
    std::sort(v_beacons.begin(),v_beacons.end());
    return v_beacons;
}

bool Datastructures::change_beacon_name(BeaconID id, const std::string& newname)
{
    if (beacons.count(id) == 1) {
        beacons.at(id).name = newname;
        return true;
    } else {
        return false;
    }
}

bool Datastructures::change_beacon_color(BeaconID id, Color newcolor)
{
    if (beacons.count(id) == 1) {
        beacons.at(id).color = newcolor;
        beacons.at(id).brightness = get_brightness(newcolor);

        minmax_b_valid = false;

        return true;
    } else {
        return false;
    }
    return false;
}

bool Datastructures::add_lightbeam(BeaconID sourceid, BeaconID targetid)
{
    if (beacons.count(sourceid) == 1 and beacons.count(targetid) == 1) {
        if (beacons.at(sourceid).lightbeam_send == NO_ID) {
            beacons.at(sourceid).lightbeam_send = targetid;
            beacons.at(targetid).lightbeams_receive.push_back(sourceid);
            return true;
        }
    }
    return false;
}

std::vector<BeaconID> Datastructures::get_lightsources(BeaconID id)
{
    std::vector<BeaconID> v_beacons{{NO_ID}};

    if (beacons.count(id) == 1) {
        v_beacons = beacons.at(id).lightbeams_receive;
    }
    std::sort(v_beacons.begin(),v_beacons.end());

    return v_beacons;
}

std::vector<BeaconID> Datastructures::path_outbeam(BeaconID id)
{
    std::vector<BeaconID> v_beacons{{NO_ID}};

    if (beacons.count(id) == 1) {
        v_beacons = {};
        v_beacons.push_back(id);
        BeaconID next_b = beacons.at(id).lightbeam_send;
        while (next_b != NO_ID) {
            v_beacons.push_back(next_b);
            next_b = beacons.at(next_b).lightbeam_send;
        }
    }

    return v_beacons;
}

bool Datastructures::remove_beacon(BeaconID id)
{
    if (beacons.count(id) == 1) {

        if (beacons.at(id).lightbeam_send != NO_ID) {
            std::vector<BeaconID> bs = beacons.at(beacons.at(id).lightbeam_send).lightbeams_receive;
            bs.erase(std::remove(bs.begin(), bs.end(), id), bs.end());
        }

        std::vector<BeaconID> br = beacons.at(id).lightbeams_receive;
        for (auto i : br) {
            beacons.at(i).lightbeam_send = NO_ID;
        }

        beacons.erase(id);

        minmax_b_valid = false;

        return true;
    } else {
        return false;
    }
}

std::vector<BeaconID> Datastructures::path_inbeam_longest(BeaconID /*id*/)
{
    // Replace this with your implementation
    return {{NO_ID}};
}

Color Datastructures::total_color(BeaconID /*id*/)
{
    // Replace this with your implementation
    return NO_COLOR;
}

std::vector<Coord> Datastructures::all_xpoints()
{
    //v_xpoints data has already been gathered; sort it here
    std::sort(v_xpoints.begin(),v_xpoints.end());
    return v_xpoints;
}

bool Datastructures::add_fibre(Coord xpoint1, Coord xpoint2, Cost cost)
{
    //hash values for the coordinates to handle the unordered map - the hashes are unique for all coordinates
    int ch1 = CoordHash().operator()(xpoint1);
    int ch2 = CoordHash().operator()(xpoint2);

    //proceed if the xpoints are not the same and the fibre that is being added doesn't already exist
    if (ch1 != ch2 and xpoints[ch1].fibres.find(ch2) == xpoints[ch1].fibres.end()) {

        //add xpoints to the vector listing them if they are not there already
        if (std::find(v_xpoints.begin(), v_xpoints.end(), xpoint1) == v_xpoints.end()) {
            v_xpoints.push_back(xpoint1);
        }
        if (std::find(v_xpoints.begin(), v_xpoints.end(), xpoint2) == v_xpoints.end()) {
            v_xpoints.push_back(xpoint2);
        }

        //add fibre to the vector listing them in a way that the smaller coordinates are first
        if (xpoint1 < xpoint2) {
            v_fibres.push_back(std::pair(xpoint1, xpoint2));
        } else {
            v_fibres.push_back(std::pair(xpoint2, xpoint1));
        }

        //add data to the container (unordered_map creates key-element pairs on the fly - similar to .insert()):

        xpoints[ch1].fibres[ch2].endx = xpoint2;
        xpoints[ch1].fibres[ch2].cost = cost;

        xpoints[ch2].fibres[ch1].endx = xpoint1;
        xpoints[ch2].fibres[ch1].cost = cost;

        //adding succeeded
        return true;
    }
    //adding didn't succeed
    return false;
}

std::vector<std::pair<Coord, Cost> > Datastructures::get_fibres_from(Coord xpoint)
{
    //hash value for the xpoint
    int chx = CoordHash().operator()(xpoint);

    std::vector<std::pair<Coord, Cost>> fibres = {};

    //iterate through the fibres of the desired xpoint and add them to the return vector
    for (auto it: xpoints[chx].fibres) {
        fibres.push_back(std::make_pair(it.second.endx, it.second.cost));
    }

    std::sort(fibres.begin(),fibres.end());

    return fibres;
}

std::vector<std::pair<Coord, Coord> > Datastructures::all_fibres()
{
    //v_fibres data has already been gathered; sort it here
    std::sort(v_fibres.begin(),v_fibres.end());
    return v_fibres;
}

bool Datastructures::remove_fibre(Coord xpoint1, Coord xpoint2)
{
    //hash values for the xpoints
    int ch1 = CoordHash().operator ()(xpoint1);
    int ch2 = CoordHash().operator ()(xpoint2);

    //create iterators that look for if the xpoints exist
    auto findx1 = xpoints.find(ch1);
    auto findx2 = xpoints.find(ch2);

    //proceed if both of the xpoints are found
    if (findx1 != xpoints.end() and findx2 != xpoints.end()) {

        //create iterator to determine if the removable fibre exists
        auto findx = xpoints[ch1].fibres.find(ch2);

        //proceed if the removable fibre exists
        if (findx != xpoints[ch1].fibres.end()) {

            //remove from the actual container and from the vector listing them:

            xpoints[ch1].fibres.erase(ch2);
            xpoints[ch2].fibres.erase(ch1);

            v_fibres.erase(std::remove(v_fibres.begin(), v_fibres.end(), std::make_pair(xpoint1,xpoint2)), v_fibres.end());
            v_fibres.erase(std::remove(v_fibres.begin(), v_fibres.end(), std::make_pair(xpoint2,xpoint1)), v_fibres.end());

            //removing succeeded
            return true;
        }
    }

    //removing didn't succeed
    return false;
}

void Datastructures::clear_fibres()
{
    //remove all data related to the fibres in the containers:

    xpoints.clear();
    v_xpoints.clear();
    v_fibres.clear();
}

std::vector<std::pair<Coord, Cost> > Datastructures::route_any(Coord fromxpoint, Coord toxpoint)
{
    //initialize a container for the possible routes
    std::vector<std::vector<std::pair<Coord, Cost>>> routes = {{std::make_pair(fromxpoint,0)}};

    int chx = CoordHash().operator ()(fromxpoint);
    std::unordered_set<int> used_xpoints = {chx};

    // max_depth of 100 is reasonable, as the randomly added fibres and queries on them can be satisfied almost always even with much greater N values
    // max_depth can be increased, but it will slow down the program for a bit
    int max_depth = 100;
    int d = 0;
    while (d < max_depth) {

        //temp-variable to hold information of new routes without messing the for-loop that handles the routes
        std::vector<std::vector<std::pair<Coord, Cost>>> new_routes = {};

        for (auto route : routes) {

            //coordinates of the last xpoint of the route and its hash
            Coord crds = route.back().first;
            int chx = CoordHash().operator ()(crds);

            //cost of the route so far
            Cost cost = route.back().second;

            //temp-variable to inspect and save different possible mutations of the common route
            std::vector<std::pair<Coord, Cost>> route_copy;

            //iterate through all fibres that are connected to the last xpoint of the route
            for (auto it: xpoints[chx].fibres) {
                
                //initialize the common route to route_copy
                route_copy = route;

                //new_crds is the seeked new xpoint
                Coord new_crds = it.second.endx;
                //hash value for new_crds
                int chnx = CoordHash().operator ()(new_crds);

                //calculate the new cost of previous cost and new location cost
                int new_cost = it.second.cost + cost;

                //if the seeked new xpoint is already in the route, don't add it
                if (used_xpoints.find(chnx) == used_xpoints.end()) {

                    //mark the xpoint as used
                    used_xpoints.insert(chnx);

                    route_copy.push_back(std::make_pair(new_crds,new_cost));

                    //if a route is found, return it immediately since it is xpoint-optimal
                    // - this way we don't need to implement route_least_xpoints as its own entity at all
                    if (new_crds == toxpoint) {
                        return route_copy;
                    }

                    new_routes.push_back(route_copy);
                }
            }
        }

        //no need to keep the old routes here
        routes = new_routes;

        ++d;
    }

    return {};
}

std::vector<std::pair<Coord, Cost>> Datastructures::route_least_xpoints(Coord fromxpoint, Coord toxpoint)
{
    //route_any was implemented to solve this function as well
    return Datastructures::route_any(fromxpoint, toxpoint);
}

std::vector<std::pair<Coord, Cost>> Datastructures::route_fastest(Coord fromxpoint, Coord toxpoint)
{
    std::vector<std::vector<std::pair<Coord, Cost>>> routes = {{std::make_pair(fromxpoint,0)}};

    // max_depth of 10 is reasonable, as the randomly added fibres and queries on them can be satisfied generally well even with much greater N values
    // max_depth can be increased, but it will slow down the program greatly in this function

    // there is no management of existing routes that would remove unoptimal routes by cost; the logic would get very complex in my container structure
    // if this kind of cleaning was implemented, the max_depth wouldn't affect the performance that much

    int max_depth = 10;
    int d = 0;
    while (d < max_depth) {

        //temp-variable to hold information of new routes without messing the for-loop that handles the routes
        std::vector<std::vector<std::pair<Coord, Cost>>> new_routes = {};

        for (auto route : routes) {

            //coordinates of the last xpoint of the route and its hash
            Coord crds = route.back().first;
            int chx = CoordHash().operator ()(crds);

            //cost of the route so far
            Cost cost = route.back().second;

            //temp-variable to inspect and save different possible mutations of the common route
            std::vector<std::pair<Coord, Cost>> route_copy;

            //iterate through all fibres that are connected to the last xpoint of the route
            for (auto it: xpoints[chx].fibres) {

                //initialize the common route to route_copy
                route_copy = route;

                //new_crds is the seeked new xpoint
                Coord new_crds = it.second.endx;

                //calculate the new cost of previous cost and new location cost
                int new_cost = it.second.cost + cost;

                //if the seeked new xpoint is already in the route, don't add it
                //find_if implemented with a lambda function since fibre consists of coordinates and cost
                // - we are interested in the coordinates here
                if (std::find_if(route.begin(), route.end(),
                                 [&](const std::pair<Coord, Cost>& fibre)
                                 { return fibre.first == new_crds;} ) == route.end()) {

                    route_copy.push_back(std::make_pair(new_crds,new_cost));
                    new_routes.push_back(route_copy);

                }

            }
        }

        //add all new routes to the existing ones
        //note that this operation increases routes size exponentially with the current logic
        // - thus small depth is advised
        for (auto route : new_routes) {
            routes.push_back(route);
        }

        ++d;
    }

    //here we find the optimal route on the given depth:

    //initialize min_route and min_cost
    std::vector<std::pair<Coord, Cost>> min_route = {};
    Cost min_cost = -1;

    for (auto route : routes) {

        //end_crds is the last xpoint in the route
        Coord end_crds = route.back().first;
        //cost is the total cost of the route
        Cost cost = route.back().second;

        //if the last xpoint is the goal and the cost is smaller than the current minimum cost to reach the goal
        if (end_crds == toxpoint and (cost < min_cost or min_cost == -1)) {

            //update min_route and min_cost with the current route
            min_cost = cost;
            min_route = route;
        }
    }

    return min_route;
}

std::vector<Coord> Datastructures::route_fibre_cycle(Coord startxpoint)
{
    //initialize a container for the possible routes
    std::vector<std::vector<Coord>> routes = {{startxpoint}};

    // max_depth of 100 is reasonable, as the randomly added fibres and queries on them can be satisfied almost always, based on probabilities
    // max_depth can be increased, but it will slow down the program in a bit
    int max_depth = 100;
    int d = 0;
    while (d < max_depth) {

        //temp-variable to hold information of new routes without messing the for-loop that handles the routes
        std::vector<std::vector<Coord>> new_routes = {};

        for (auto route : routes) {

            //coordinates of the last xpoint of the route and its hash
            Coord crds = route.back();
            int chx = CoordHash().operator()(crds);

            //temp-variable to inspect and save different possible mutations of the common route
            std::vector<Coord> route_copy;

            //iterate through all fibres that are connected to the last xpoint of the route
            for (auto it: xpoints[chx].fibres) {

                //initialize the common route to route_copy
                route_copy = route;

                //new_crds is the seeked new xpoint
                Coord new_crds = it.second.endx;

                //If the seeked new xpoint is already in the route, don't add it
                if (std::find(route.begin(), route.end(), new_crds) == route.end()) {

                    route_copy.push_back(new_crds);
                    new_routes.push_back(route_copy);

                } else if (route.size() >= 2) {

                    //If the seeked new xpoint is already in the route and isn't the previous xpoint, a cycle has been found - return the route
                    if (std::find(route.begin(), route.end(), new_crds) != route.begin() + (route.size() - 2)) {
                        route.push_back(new_crds);
                        return route;
                    }
                }
            }
        }

        //no need to keep the old routes here
        routes = new_routes;

        ++d;
    }

    return {};
}

Cost Datastructures::trim_fibre_network()
{
    // Replace this with your implementation
    return NO_COST;
}
