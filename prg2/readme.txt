TIE-20100 Tietorakenteet ja algoritmit, kevät 2019
Harjoitustyö 2:
Game of Fibres
Joni Seppälä 246014

Toteutin kaikki pakolliset osat ohjelmasta ja lisäksi toteutin jäsenfunktiot route_least_xpoints, route_fastest ja route_fibre_cycle; tarkoittaen että en toteuttanut vapaavalintaista trim_fibre_network.

Toteutuksessani hyödynsin usein valmiina tarjotun CoordHash-structin toiminnallisuutta; funktiolla muutetaan koordinaatteja uniikeiksi int-muuttujiksi, joita käytin lukuisten unordered_map-säiliöiden avaimina. unordered_map on hyvä säiliö useisin ohjelman osiin sen asymptoottisten nopeuksien vuoksi - melkein aina keskimäärin O(1). Käytin vectoreita silloin, kun haluttu return arvo oli tätä muotoa tai kun unordered_mapin tai unordered_setin käyttäminen aiheutti liikaa vaikeuksia. vectoreiden asymptoottinen nopeus on useissa operaatioissa O(n), mikä ei ole yhtä hyvä kuin mainituilla vaihtoehdoilla.

Primäärinen säiliö kuitujen ja risteyksien tiedoille on unordered_map xpoints. Avainosa saadaan mainitulla hash-toiminnallisuudella. Dataosa sisältää risteykset (xpoint_), jotka puolestaan sisältävät niistä lähtevät kuidut (fibre_). Nämä kuidut (fibre_) sisältävät tiedon niiden toisen pään koordinaateista ja hinnan.

Primäärisen säiliön lisäksi privatessa määrittelin kaksi vektoria ohjelman sisäisiä kyselyitä ja tulostuksia varten: v_xpoints ja v_fibres. Nämä ovat sinänsä redundantteja primäärisen säiliön kanssa; niitä päivitetään samalla kun primäärisenkin säiliön tietoja päivitetään, mutta ne ovat suoraan siinä muodossa, kuin niitä palauttavat funktiot niitä tarvitsevat, jolloin tietoja kysyttäessä ei tarvitse tehdä muita toimenpiteitä kuin kerättyjen tietojen sorttaaminen. Toisaalta näiden vektorien ylläpitäminen aiheuttaa sen, että O(1) vaihtui kahdessa jäsenfunktiossa O(n). KOkonaisuudessa uskon tämän ratkaisun kuitenkin nopeuttaneen ohjelman toimintaa.

Toteutin route_any() logiikan niin, että se tuottaa oikean reitin aina myös route_least_xpoints() -jäsenfunktioon, jolloin jälkimmäinen voidaan hoitaa kutsumalla ensimmäistä funktiota. route_fibre_cycle() on oma toteutuksensa, joka muistuttaa funktiota route_any() logiikaltaan, mutta poikkeaa hieman. Asetin kaikille route-funktioille, paitsi route_fastest(), etsintäsyvyyden katoksi 100. Tämän voi perustella sillä, että erittäin suurillakin säikeiden määrällä satunnaisesti lisättävät säikeet ja satunnaisesti tehtävät kyselyt tuottavat hyvin epätodennäköisesti reitin, jolle tämä syvyys ei riittäisi. Mikäli tämä syvyys ei kuitenkaan ole tarpeeksi suuri, voidaan maksimisyvyys-vakiota aina kasvattaa. Tämä hidastaa funktioiden suoritumisaikoja niissä tapauksissa, missä maalia ei voida saavuttaa - reittiä tai sykliä ei löydy. Lopulliset asymptoottiset notaatiot eivät ole kovin lupaavia, mutta ovat pathfindingille tyypillisiä.

route_fastest() tuotti vaikeuksia toteuttaa järkevästi. Ymmärrän idean, miten löydetään optimaalinen reitti, mutta sen toteuttaminen valitsemillani tietorakenteilla tuotti niin paljon vaikeuksia, että päädyin ratkaisuun, joka kyllä toimii, mutta kasvattaa reittien säiliötä eksponentiaalisesti etsinnän syvyyden kasvaessa. Suoritusajan järkevöittämiseksi asetin syvyyden katoksi 10, mikä tarkoittaa, että läheskään aina ei löydetä optimaalista reittiä määränpäähän, mutta sentään funktio suoritetaan aina järkevässä ajassa. Syvyyttä kasvattamalla löydetään pidempiä optimaalisia reittejä mutta samalla kasvatetaan funktion suorittamiseen kuluvaa aikaa. Lisäksi lopulta jouduin leimaamaan tälle funktiolle asymptoottiseksi notaatioksi hirvittävän O(n^3). Lienee mahdollista, että route_fastest() saisi kertaluokkaan O(n^2), mutten itse onnistunut tässä.

Alla on suoriutumistesti pakollisilla ominaisuuksilla:

> perftest compulsory 100 1 10;100;1000;3000;10000;30000;100000;300000;1000000
Timeout for each N is 100 sec. 
For each N perform 1 random command(s) from:
all_xpoints change_color change_name fibres lightsources max_brightness min_brightness path_outbeam random_add random_fibres remove_fibre route_any route_fastest route_fibre_cycle route_least_xpoints sort_alpha sort_brightness trim_fibre_network 

      N ,    add (sec) ,   cmds (sec) ,  total (sec)
     10 ,   5.5012e-05 ,   1.3426e-05 ,   6.8438e-05
    100 ,  0.000763467 ,  0.000593604 ,   0.00135707
   1000 ,    0.0121748 ,   6.4964e-05 ,    0.0122398
   3000 ,    0.0624411 ,   4.1139e-05 ,    0.0624822
  10000 ,     0.507592 ,   5.2236e-05 ,     0.507645
  30000 ,      3.77968 ,  0.000157119 ,      3.77984
 100000 ,      39.8808 ,   3.2408e-05 ,      39.8808
 300000 , Timeout!

Testistä huomaa, että N kasvaessa suoritusaika kasvaa suurinpiirtein N*log(N) verran.

Suoriutumistesti route_least_xpoints:

> perftest route_least_xpoints 100 1 10;100;1000;3000;10000;30000;100000;300000;1000000
Timeout for each N is 100 sec. 
For each N perform 1 random command(s) from:
route_least_xpoints 

      N ,    add (sec) ,   cmds (sec) ,  total (sec)
     10 ,   5.5921e-05 ,   4.7524e-05 ,  0.000103445
    100 ,  0.000811053 ,   3.3544e-05 ,  0.000844597
   1000 ,    0.0123208 ,   0.00239712 ,    0.0147179
   3000 ,    0.0613595 ,   0.00463541 ,    0.0659949
  10000 ,     0.475811 ,   6.2902e-05 ,     0.475874
  30000 ,      3.80081 ,   8.2037e-05 ,      3.80089
 100000 ,      39.4081 ,     0.205839 ,      39.6139
 300000 , Timeout!

N ollessa pieni (<10000) suoritusaika kasvaa N*log(N); tämän jälkeen suoritusaika lähtee kasvamaan paljon jyrkemmin.

route_fastest ja route_fibre_cycle suoriutuivat lähes identtisesti. route_fastest tulos on täten yllättävä, sillä oletin sen suoriutuvan muita hitaammin - pienen maksimisyvyysmuuttujan vaikutus on tässä luultavasti merkittävä.

> perftest route_fastest 100 1 10;100;1000;3000;10000;30000;100000;300000;1000000
Timeout for each N is 100 sec. 
For each N perform 1 random command(s) from:
route_fastest 

      N ,    add (sec) ,   cmds (sec) ,  total (sec)
     10 ,    5.931e-05 ,   4.0996e-05 ,  0.000100306
    100 ,  0.000823005 ,   7.9978e-05 ,  0.000902983
   1000 ,    0.0121934 ,   9.2463e-05 ,    0.0122859
   3000 ,    0.0598208 ,   9.8992e-05 ,    0.0599198
  10000 ,     0.476563 ,     0.169925 ,     0.646488
  30000 ,      3.72682 ,   0.00613572 ,      3.73296
 100000 ,      39.8949 ,   4.2949e-05 ,       39.895
 300000 , Timeout!

> perftest route_fibre_cycle 100 1 10;100;1000;3000;10000;30000;100000;300000;1000000
Timeout for each N is 100 sec. 
For each N perform 1 random command(s) from:
route_fibre_cycle 

      N ,    add (sec) ,   cmds (sec) ,  total (sec)
     10 ,    5.227e-05 ,   4.0127e-05 ,   9.2397e-05
    100 ,  0.000847962 ,   5.6929e-05 ,  0.000904891
   1000 ,    0.0121598 ,   4.3205e-05 ,     0.012203
   3000 ,    0.0623639 ,  0.000397024 ,    0.0627609
  10000 ,     0.482679 ,  0.000184331 ,     0.482863
  30000 ,      3.78819 ,   5.2079e-05 ,      3.78824
 100000 ,      39.6492 ,   6.4427e-05 ,      39.6493
 300000 , Timeout!
