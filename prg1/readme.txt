TIE-20100 Tietorakenteet ja algoritmit, kevät 2019
Harjoitustyö 1:
Beacons of RGB
Joni Seppälä 246014

Toteutin kaikki pakolliset osat ohjelmasta ja lisäksi toteutin jäsenfunktion remove_beacon.

Valitsin tietorakenteeksi unordered_mapin, sillä se on pääsee keskimäärin O(1) kompleksisuuteen siihen kohdistuvissa operaatioissa.
Erityisen kätevää monien toimintojen kannalta on se, että majakoiden ID:tä voi käyttää tietorakenteen avaimena.
Unordered_map sisältö koostuu structeista, jotka sisältävät varsinaiset majakoiden tiedot: nimen, koordinaatit, värin, väristä lasketun kirkkauden ja lisäksi majakoiden IDt, jotka lähettävät valoa majakalle / jolle majakka lähettää valoa. Tällainen keskitetty tiedon tallentaminen tuntui loogiselta.

Toimintojen pullonkauloiksi muodostui useasti vaadittu vektorien sorttaaminen. Toteutin kaikki sorttaamiset std::sortilla, jonka aikakompleksisuus on O(n*log(n)).
Parempaan aikakompleksisuuteen ei olisi päästy muilla sorttausalgoritmeilla, joskin harkitsin silti merge_sortin toteuttamista.

Pidän monia toteutuksiani melko hyvin optimoituina. Esimerkiksi majakoiden kirkkaukset tallennetaan eikä lasketa aina uudestaan ja kirkkain/himmein majakka tallennetaan aina, kun majakat pyydetään niiden kirkkausjärjestyksessä. Näitä arvoja voidaan käyttää, kunnes ne invalidoituvat majakoita lisättäessä, poistettaessa tai niiden värien muokkauksen yhteydessä.

Alla on suoriutumistesti pakollisilla ominaisuuksilla:

> perftest compulsory 100 1 10;100;1000;10000;100000;1000000;10000000
Timeout for each N is 100 sec. 
For each N perform 1 random command(s) from:
change_color change_name lightsources max_brightness min_brightness path_outbeam random_add sort_alpha sort_brightness 

      N ,    add (sec) ,   cmds (sec) ,  total (sec)
     10 ,   8.2755e-05 ,   7.5513e-05 ,  0.000158268
    100 ,  0.000467797 ,   1.7236e-05 ,  0.000485033
   1000 ,   0.00503841 ,   3.6411e-05 ,   0.00507482
  10000 ,    0.0572358 ,     0.119795 ,     0.177031
 100000 ,     0.641207 ,   3.2691e-05 ,     0.641239
1000000 ,      7.45761 ,      24.2007 ,      31.6583
10000000 ,      77.7226 , Timeout!

Testistä huomaa, että suoritusaika kasvaa suurinpiirtein lineaarisesti N kasvaessa.
