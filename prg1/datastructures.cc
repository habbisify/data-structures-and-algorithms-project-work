// Datastructures.cc

#include "datastructures.hh"

#include <random>
#include <algorithm>
#include <cmath>

std::minstd_rand rand_engine; // Reasonably quick pseudo-random generator

template <typename Type>
Type random_in_range(Type start, Type end)
{
    auto range = end-start;
    ++range;

    auto num = std::uniform_int_distribution<unsigned long int>(0, range-1)(rand_engine);

    return static_cast<Type>(start+num);
}

Datastructures::Datastructures()
{
    //Values that are lower and higher than the possible min and max
    max_b = make_pair(NO_ID,-1);
    min_b = make_pair(NO_ID,2551);

    minmax_b_valid = false;
}

Datastructures::~Datastructures()
{
    clear_beacons();
}

int Datastructures::beacon_count()
{
    int s = beacons.size();
    if (s > 0) {
        return s;
    } else {
        return NO_VALUE;
    }
}

void Datastructures::clear_beacons()
{
    beacons.clear();

    //Need to reset the private variables
    max_b = make_pair(NO_ID,-1);
    min_b = make_pair(NO_ID,2551);

    minmax_b_valid = false;
}

std::vector<BeaconID> Datastructures::all_beacons()
{
    std::vector<BeaconID> v_beacons{};
    for ( auto it = beacons.begin(); it != beacons.end(); ++it ) {
        v_beacons.push_back(it->first); //first is the ID of the beacon
    }
    return v_beacons;
}

int Datastructures::get_brightness(Color color) {

    int brightness = 3*color.r + 6*color.g + color.b; //rough estimate on how a human sees the color brightness

    return brightness;
}

bool Datastructures::add_beacon(BeaconID id, const std::string& name, Coord xy, Color color)
{
    if (beacons.count(id) == 0) {

        Beacon beacon;
        beacon.name = name;
        beacon.crds = xy;
        beacon.color = color;

        beacon.brightness = get_brightness(color);
        minmax_b_valid = false;

        beacon.lightbeam_send = NO_ID;
        beacon.lightbeams_receive = {};

        beacons.insert(make_pair(id, beacon));
        return true;
    } else {
        return false;
    }
}

std::string Datastructures::get_name(BeaconID id)
{
    //beacons.contains(id) would be the more modern approach but isn't available until c++20
    if (beacons.count(id) == 1) {
        return beacons.at(id).name;
    } else {
        return NO_NAME;
    }
}

Coord Datastructures::get_coordinates(BeaconID id)
{
    if (beacons.count(id) == 1) {
        return beacons.at(id).crds;
    } else {
        return NO_COORD;
    }
}

Color Datastructures::get_color(BeaconID id)
{
    if (beacons.count(id) == 1) {
        return beacons.at(id).color;
    } else {
        return NO_COLOR;
    }
}

std::vector<BeaconID> Datastructures::beacons_alphabetically()
{
    std::vector<BeaconID> v_beacons = all_beacons();

    //The & in the square brackets gives the lambda function private variables of Datastructures making the comparison possible
    std::sort(v_beacons.begin(), v_beacons.end(), [&](BeaconID left, BeaconID right) -> bool
    {
        return beacons.at(left).name < beacons.at(right).name;
    });
    return v_beacons;
}

std::vector<BeaconID> Datastructures::beacons_brightness_increasing()
{
    std::vector<BeaconID> v_beacons = all_beacons();

    std::sort(v_beacons.begin(), v_beacons.end(), [&](BeaconID left, BeaconID right) -> bool
    {
        return beacons.at(left).brightness < beacons.at(right).brightness;
    });

    //Max and min brightness are gotten "free" here
    if (v_beacons.size() > 0) {
        min_b.first = v_beacons.front();
        min_b.second = beacons.at(v_beacons.front()).brightness;
        max_b.first = v_beacons.back();
        max_b.second = beacons.at(v_beacons.back()).brightness;
    }
    minmax_b_valid = true;

    return v_beacons;
}

BeaconID Datastructures::min_brightness()
{
    if (!minmax_b_valid) {
        beacons_brightness_increasing(); //updates the min_b variable
    }
    return min_b.first;
}

BeaconID Datastructures::max_brightness()
{
    if (!minmax_b_valid) {
        beacons_brightness_increasing(); //updates the max_b variable
    }
    return max_b.first;
}

std::vector<BeaconID> Datastructures::find_beacons(std::string const& name)
{
    std::vector<BeaconID> v_beacons{};
    for ( auto it = beacons.begin(); it != beacons.end(); ++it ) {
        if (it->second.name == name) {
            v_beacons.push_back(it->first);
        }
    }
    std::sort(v_beacons.begin(),v_beacons.end());
    return v_beacons;
}

bool Datastructures::change_beacon_name(BeaconID id, const std::string& newname)
{
    if (beacons.count(id) == 1) {
        beacons.at(id).name = newname;
        return true;
    } else {
        return false;
    }
}

bool Datastructures::change_beacon_color(BeaconID id, Color newcolor)
{
    if (beacons.count(id) == 1) {
        beacons.at(id).color = newcolor;
        beacons.at(id).brightness = get_brightness(newcolor);

        minmax_b_valid = false;

        return true;
    } else {
        return false;
    }
    return false;
}

bool Datastructures::add_lightbeam(BeaconID sourceid, BeaconID targetid)
{
    if (beacons.count(sourceid) == 1 and beacons.count(targetid) == 1) {
        if (beacons.at(sourceid).lightbeam_send == NO_ID) {
            beacons.at(sourceid).lightbeam_send = targetid;
            beacons.at(targetid).lightbeams_receive.push_back(sourceid);
            return true;
        }
    }
    return false;
}

std::vector<BeaconID> Datastructures::get_lightsources(BeaconID id)
{
    std::vector<BeaconID> v_beacons{{NO_ID}};

    if (beacons.count(id) == 1) {
        v_beacons = beacons.at(id).lightbeams_receive;
    }
    std::sort(v_beacons.begin(),v_beacons.end());

    return v_beacons;
}

std::vector<BeaconID> Datastructures::path_outbeam(BeaconID id)
{
    std::vector<BeaconID> v_beacons{{NO_ID}};

    if (beacons.count(id) == 1) {
        v_beacons = {};
        v_beacons.push_back(id);
        BeaconID next_b = beacons.at(id).lightbeam_send;
        while (next_b != NO_ID) {
            v_beacons.push_back(next_b);
            next_b = beacons.at(next_b).lightbeam_send;
        }
    }

    return v_beacons;
}

bool Datastructures::remove_beacon(BeaconID id)
{
    if (beacons.count(id) == 1) {

        if (beacons.at(id).lightbeam_send != NO_ID) {
            std::vector<BeaconID> bs = beacons.at(beacons.at(id).lightbeam_send).lightbeams_receive;
            bs.erase(std::remove(bs.begin(), bs.end(), id), bs.end());
        }

        std::vector<BeaconID> br = beacons.at(id).lightbeams_receive;
        for (auto i : br) {
            beacons.at(i).lightbeam_send = NO_ID;
        }

        beacons.erase(id);

        minmax_b_valid = false;

        return true;
    } else {
        return false;
    }
}

std::vector<BeaconID> Datastructures::path_inbeam_longest(BeaconID /*id*/)
{
    // Replace this with your implementation
    return {{NO_ID}};
}

Color Datastructures::total_color(BeaconID /*id*/)
{
    // Replace this with your implementation
    return NO_COLOR;
}
